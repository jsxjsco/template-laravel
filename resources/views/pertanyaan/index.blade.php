@extends('adminlte.master')

@section('content')
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">List Pertanyaan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <a class="btn btn-primary mb-2" href="/pertanyaan/create">Buat Komen Baru</a>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="width: 10px">No</th>
                    <th>Judul Pertanyaan</th>
                    <th>Isi Pertanyaan</th>
                    <th style="width: 40px">Actioan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($pertanyaan as $key => $pertanyaan)
                  <tr>
                      <td> {{$key +1}}</td>
                      <td> {{$pertanyaan-> judul}}</td>
                      <td> {{$pertanyaan-> isi}}</td>
                      <td style="display: flex;">
                          <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm">show</a>
                          <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-info btn-sm">edit</a>
                          <form action="/pertanyaan/{{$pertanyaan->id}}" method="post">
                            @scrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                          </form>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
@endsection